import React from 'react';
import './App.css';

import data from './data.json'

/*************************************************************** 
 * Name:        ToggleDrop
 * 
 * Description: Dropdown Menu
 * 
 * Props:       title - dropdown title
 *              data  - data to be diplayed when expanded
***************************************************************/
class Dropdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isTogggleOn: true}
    
    // Allows `this` to work in callback
    // Generally, if you refer to a method without () after it, such as onClick={this.handleClick}, you should bind that method.
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick() {
    this.setState(prevState => ({
      isTogggleOn: !prevState.isTogggleOn
    }));
  }

  render() {
    const isCollapsed = this.state.isTogggleOn;
    let dropdown;

    if (isCollapsed) {
      dropdown = (
        <li>
          <div className='DropTitle'>{this.props.title}</div>
          <button className='DropBut' onClick={this.handleClick}>v</button>
        </li>
      );
    } 
    else {
      dropdown = (
        <li>
          <div className='DropTitle'>{this.props.title}</div>
          <button className='DropBut' onClick={this.handleClick}>^</button>
          <ul className='SubTasks'>
            {this.props.data.map((sub) => <li>{sub}</li>)}
          </ul>
        </li>
      );
    }

    return (dropdown);
  }
}

/*************************************************************** 
 * Name:        Task
 * 
 * Description: Creates unorder list of tasks
 * 
 * Props:       tasks - array of tasks to added to list
***************************************************************/
function TaskList(props) {
  const taskItems = props.tasks.map((task) => <Dropdown title={task.title} data={task.subTasks}/>);
  
  return (
    <ul className='Tasks'>{taskItems}</ul>
  );
}

/*************************************************************** 
 * Name:        App
 * 
 * Description: Root Component
 * 
 * Props:       None
***************************************************************/
function App() {
  return (
    <div className='App'>
      <header className='App-header'>
        <h1>Task List</h1>
        
        <TaskList className='Tasks' tasks={data.tasks}/>
      </header>
    </div>
  );
}

export default App;
